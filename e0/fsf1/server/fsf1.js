// fsf1.js

// 1) Server app running (able to app.listen)
// 1a) Test Server-DB connection
// -------------------
// 2) Paper plan of app dev'pmt
// 2a) Paper plan of HTML->HTML UI-Routing
// 2b) Paper plan of HTML->CTRL->SVC->Server->DB flow
// 2c) Setup UI-Router Config $state
// 2d) Test Index.html $state.go - Create.html & Read/Retrieve.html
// -------------------
// 3) Create - a) HTML fields naming === b) CTRL naming === c) SVC naming d) Models (Data models) === Server API === DB column names
// 3a) ************************************** check the data objects passing carefully

// 4) Test single record creation into DB
// 5) Read/Retrieve - single record a) HTML page, b) CTRL, c) SVC, d) Server API
// 6) Delete single record a) HTML, b) CTRL, c) SVC, d) Server API
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// 7) Test multi-table JOINS
// 8) Update/Edit a) HTML, b) CTRL, c) SVC, d) Server API
// Tidy up HTMLs (validations, messages), other functions - File uploads


// DEPENDENCIES ------------------------------------------------------------------------------------------
var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");
var mysql = require("mysql");
var mongoose = require("mongoose");


// CONSTANTS --------------------------------------------------------------------------------------------
// Defines server port.
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
const NODE_PORT = process.env.PORT || 3000;

// Defines paths
// __dirname is a global that holds the directory name of the current module
const CLIENT_FOLDER = path.join(__dirname + '/../client');  // CLIENT FOLDER is the public directory
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');

const API_DEPT_ENDPT = "/api/departments";
const API_LOGIN_ENDPT = "/api/login";

const MYSQL_USERNAME = "hb";
const MYSQL_PASSWORD = "root777";
const DB1 = "fsf1db_sql";

// OTHER VARS ---------------------------------------------------------------------------------------------------------
// Creates an instance of express called app
var app = express(); 

// DBs, MODELS, and ASSOCIATIONS ---------------------------------------------------------------------------------------
// Creates a MySQL connection
// var sequelize = ""

// if (process.env.CLEARDB_DATABASE_URL != '') {
//      sequelize = new Sequelize(
//         process.env.CLEARDB_DATABASE_URL,
//         {
//             logging: console.log,
//             dialect: 'mysql',
//             pool: {
//                 max: 5,
//                 min: 0,
//                 idle: 10000
//             }
//         }
//     );

// }
// else {
     var sequelize = new Sequelize(
        DB1,                           // 'fsf1db_sql',                    // MySQL database
        MYSQL_USERNAME,
        MYSQL_PASSWORD,
        {
            host: 'localhost',         // default port    : 3306
            logging: console.log,
            dialect: 'mysql',
            pool: {
                max: 5,
                min: 0,
                idle: 10000
            }
        }
    );

sequelize.authenticate()
    .then(function() {
        console.log("SVR >> DB %s authenticated", DB1);
    })
    .catch(function(err) {
        console.log("SVR >> DB failure: ", err);
    });


// }
// Loads model for grocery_list table
// var Product = require('./models/product')(sequelize, Sequelize);
// var Employee = require('./models/employee')(sequelize, Sequelize);
// var Manager = require('./models/deptmanager')(sequelize, Sequelize);

// Associations. Reference: https://dev.mysql.com/doc/employee/en/sakila-structure.html
// Link Department model to DeptManager model through the dept_no FK. This relationship is 1-to-N and so we use hasMany
// Link DeptManager model to Employee model through the emp_no FK. This relationship is N-to-1 and so we use hasOne
// Department.hasMany(Manager, {foreignKey: 'dept_no'});
// Manager.hasOne(Employee, {foreignKey: 'emp_no'});

// MIDDLEWARES --------------------------------------------------------------------------------------------

// Serves files from public directory (in this case CLIENT_FOLDER).
// __dirname is the absolute path of the application directory.
// if you have not defined a handler for "/" before this line, server will look for index.html in CLIENT_FOLDER
app.use(express.static(CLIENT_FOLDER));

// Populates req.body with information submitted through the registration form.
// Default $http content type is application/json so we use json as the parser type
// For content type is application/x-www-form-urlencoded  use: app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());


// ROUTE HANDLERS -----------------------------------------------------------------------------------------------------

// Defines endpoint handler exposed to client side for registration
app.post("/api/departments", function (req, res) {
    // Information sent via an HTTP POST is found in req.body
    console.log('Server >> POST 1  \nData Submitted');
    console.log('Dept No: ' + req.body.dept.id);
    console.log('Dept Name: ' + req.body.dept.name);

    /* // This statement creates a record in the departments table. We are commenting this out becuase of a new
     // requirement where when a record is created in departments, an equivalent record must be written in dept_manager
     // Keeping this for future reference
     Department
     .create({
     dept_no: req.body.dept.id,
     dept_name: req.body.dept.name
     })
     .then(function (department) {
     console.log(department.get({plain: true}));
     res
     .status(200)
     .json(department);
     })
     .catch(function (err) {
     console.log("error: " + err);
     res
     .status(500)
     .json(err);

     });
     */

    sequelize
        .transaction(function (t) {
            return Department
                .create(
                    {
                        dept_no: req.body.dept.id
                        , dept_name: req.body.dept.name
                    }
                    , {transaction: t})
                .then(function (department) {
                    console.log('Server >> POST 2 Sequelize Success');
                    console.log("inner result " + JSON.stringify(department))
                    return Manager
                        .create(
                            {
                                dept_no: req.body.dept.id
                                , emp_no: req.body.dept.manager
                                , from_date: req.body.dept.from_date
                                , to_date: req.body.dept.to_date
                            }
                            , {transaction: t});
                });
        })
        .then(function (results) {
            res
                .status(200)
                .json(results);
        })
        .catch(function (err) {
            console.log('Server >> POST 3 Sequelize Fail');
            console.log("outer error: " + JSON.stringify(err));
            res
                .status(500)
                .json(err);
        });


});

// Defines endpoint handler exposed to client side for retrieving login information from database
app.post(API_LOGIN_ENDPT, function (req, res) {
    console.log('SVR >> POST /api/login searchStr:', req.query.searchString);
    LoginModel
    // findAll asks sequelize to search
    // Post.findAll({
    //     where: {
    //         authorId: 12,
    //         status: 'active'
    //     }
    // });
        .findAll({
            where: {
                loginID: {$like: "%" + req.query.searchString + "%"},
                loginEmail: {$like: "%" + req.query.searchString + "%"},
                loginPassword: {$like: "%" + req.query.searchString + "%"}
            },
            // offset: 15,
            // limit: 20
        })
        .then(function (loginok) {
            console.log('SVR >> POST /api/login Success: ', loginok);
            res
                .status(200)
                .json(loginok);
        })
        .catch(function (err) {
            console.log('SVR >> POST /api/login Fail');
            res
                .status(500)
                .json(err);
        });
});



// Defines endpoint handler exposed to client side for retrieving department information from database
app.get("/api/products", function (req, res) {
    console.log('Server >> GET 4');
    console.log('Server >> GET searchStr:', req.query.searchString);
    Product
    // findAll asks sequelize to search
        .findAll({  // .findAndCountAll    result.count $gt 20
            where: {
                $or: [
                    {name: {$like: "%" + req.query.searchString + "%"}},
                    {brand: {$like: "%" + req.query.searchString + "%"}}
                ]
            },
            // offset: 15,
            limit: 20 // for testing.... revert to 20
        })
        .then(function (products) {
            console.log('Server >> GET 5 Success');
            console.log('Server >> products: ', products);
            res
                .status(200)
                .json(products);
        })
        .catch(function (err) {
            console.log('Server >> GET 6 Fail');
            res
                .status(500)
                .json(err);
        });
});


app.get("/api/departments/managers", function (req, res) {
    console.log('Server >> GET 7');
    Department
    // Use findAll to retrieve multiple records
        .findAll({
            // Use the where clause to filter final result; e.g., when you only want to retrieve departments that have
            // "s" in its name
            where: {
                // $or operator tells sequelize to retrieve record that match any of the condition
                $or: [
                    // $like + % tells sequelize that matching is not a strict matching, but a pattern match
                    // % allows you to match any string of zero or more characters
                    {dept_name: {$like: "%" + req.query.searchString + "%"}},
                    {dept_no: {$like: "%" + req.query.searchString + "%"}}
                ]
            }
            
            , include: [{
                model: Manager
                , order: [["to_date", "DESC"]]
                , limit: 1
                // We include the Employee model to get the manager's name
                , include: [Employee]
            }]
        })
        
        .then(function (departments) {
            console.log('Server >> GET 8 Success');
            res
                .status(200)
                .json(departments);
        })
        // this .catch() handles erroneous findAll operation
        .catch(function (err) {
            console.log('Server >> GET 9 Fail');
            res
                .status(500)
                .json(err);
        });
});


app.get("/api/departments/:dept_no", function (req, res) {
    console.log('Server >> GET 10');
    var where = {};
    if (req.params.dept_no) {
        where.dept_no = req.params.dept_no
    }

    console.log("Server >> GET 11 -> where: " + where);
    // We use findOne because we know (by looking at the database schema) that dept_no is the primary key and
    // is therefore unique. We cannot use findById because findById does not support eager loading
    Department
        .findOne({
            where: where
            
            , include: [{
                model: Manager
                , order: [["to_date", "DESC"]]
                , limit: 1
                // We include the Employee model to get the manager's name
                , include: [Employee]
            }]
        })
        
        .then(function (departments) {
            console.log("Server >> GET 12 Success");
            console.log("-- GET /api/departments/:dept_no findOne then() result \n " + JSON.stringify(departments));
            res.json(departments);
        })
        // this .catch() handles erroneous findAll operation
        .catch(function (err) {
            console.log("Server >> GET 13 Fail");
            console.log("-- GET /api/departments/:dept_no findOne catch() \n " + JSON.stringify(departments));
            res
                .status(500)
                .json({error: true});
        });

});


// -- Updates department info
app.put('/api/update/:name', function (req, res) {
    console.log("Server >> PUT 14");
    var where = {};
    where.name = req.params.name;

    // Updates department detail
    Product
        .update(
            {brand: req.body.brand}             // assign new values
            , {where: where}                            // search condition / criteria
        )
        .then(function (response) {
            console.log("Server >> PUT 15 Success");
            console.log("-- PUT /api/update/:name grocery_list.update then(): \n"
                + JSON.stringify(response));
        })
        .catch(function (err) {
            console.log("Server >> PUT 16 Fail");
            console.log("-- PUT /api/update/:name grocery_list.update catch(): \n"
                + JSON.stringify(err));
        });
});

// -- Searches for and deletes manager of a specific department.
// Client sent data as route parameters, we access route parameters (named routes) via the req.params property
// app.delete("/api/departments/:dept_no/managers/:emp_no", function (req, res) {
//     console.log("Server >> DEL ln 356");    
//     var where = {};
//     where.dept_no = req.params.dept_no;
//     where.emp_no = req.params.emp_no;

//     // The dept_manager table's primary key is a composite of dept_no and emp_no
//     // We will use these to find our manager. It is important to include dept_no because an employee maybe a
//     // manager of 2 or more departments. Even if business logic doesn't support this, always search
//     // a table and delete rows of a table based on the defined primary keys
//     Manager
//         .destroy({
//             where: where
//         })
//         .then(function (result) {
//             console.log("Server >> DEL ln 370 Success");    
//             if (result == "1")
//                 res.json({success: true});
//             else
//                 res.json({success: false});
//         })
//         .catch(function (err) {
//             console.log("Server >> DEL ln 377 Fail");    
//             console.log("-- DELETE /api/managers/:dept_no/:emp_no catch(): \n" + JSON.stringify(err));
//         });
// });


// Defines endpoint handler exposed to client side for retrieving all department information (static)
app.get("/api/static/departments", function (req, res) {
    console.log("Server >> GET 17");    
    // Departments contain all departments and is the data returned to client
    var departments = [
        {
            deptNo: 1001,
            deptName: 'Admin'
        }
        , {
            deptNo: 1002,
            deptName: 'Finance'
        }
        , {
            deptNo: 1003,
            deptName: 'Sales'
        }
        , {
            deptNo: 1004,
            deptName: 'HR'
        }
        , {
            deptNo: 1005,
            deptName: 'Staff'
        }
        , {
            deptNo: 1006,
            deptName: 'Customer Care'
        }
        , {
            deptNo: 1007,
            deptName: 'Support'
        }
    ];

    // Return departments as a json object
    res.status(200).json(departments);
});

// Defines endpoint handler exposed to client side for retrieving employees that are non-managers. Duet to large number
// of records in the employees database, this function limits the number of records retrieved
app.get("/api/employees", function (req, res) {
    console.log("Server >> GET 18");    
    
    sequelize
        .query("SELECT emp_no, first_name, last_name " +
            "FROM employees e " +
            "WHERE NOT EXISTS " +
            "(SELECT * " +
            "FROM dept_manager dm " +
            "WHERE dm.emp_no = e.emp_no )" +
            "LIMIT 100; "
        )
        // this .spread() handles successful native query operation
        // we use .spread instead of .then so as to separate metadata from the emplooyee records
        .spread(function (employees) {
            console.log("Server >> GET 19 Sequelize spread");    
            res
                .status(200)
                .json(employees);
        })
        // this .catch() handles erroneous native query operation
        .catch(function (err) {
            console.log("Server >> GET 20 Sequelize Fail");    
            res
                .status(500)
                .json(err);
        });
});


// ERROR HANDLING ----------------------------------------------------------------------------------------------------
// Handles 404. In Express, 404 responses are not the result of an error,
// so the error-handler middleware will not capture them.
// To handle a 404 response, add a middleware function at the very bottom of the stack
// (below all other path handlers)
app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// Error handler: server error
app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});


// SERVER / PORT SETUP ------------------------------------------------------------------------------------------------
// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("fsf1 SVR: %s at http://localhost:%d", new Date(), NODE_PORT);
});


